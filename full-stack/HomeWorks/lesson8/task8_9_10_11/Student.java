package HomeWorks.lesson8.task8_9_10_11;

public class Student extends UniversityManagmentSystem{

    public String subject;
    public Student(String fullname, int age, String subject) {
        super(fullname, age);
        this.subject=subject;
    }
}
