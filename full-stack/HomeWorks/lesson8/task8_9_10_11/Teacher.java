package HomeWorks.lesson8.task8_9_10_11;

public class Teacher extends UniversityManagmentSystem{
    public String majority;

    public Teacher(String fullname, int age, String majority) {
        super(fullname, age);
        this.majority = majority;
    }
}
